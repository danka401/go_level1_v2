package main

import (
	"fmt"
	"math"
)

func main() {
	var s float64
	for {
		fmt.Println("Введите площадь круга")
		_, _ = fmt.Scanf("%f\n", &s)
		if s <= 0 {
			fmt.Print("Число долно быть больше нуля. ")
		} else {
			break
		}
	}

	var d, l float64
	d = 2 * math.Sqrt(s/math.Pi)
	l = d * math.Pi
	fmt.Printf("диаметр круга равен %f, а длинна окружности %f\n", d, l)
}

package main

import (
	"fmt"
)

func main() {
	//var digits = -1
	//for digits < 0 || digits > 999 {
	//	fmt.Println("Введите число от 0 до 999")
	//	_, _ = fmt.Scanf("%d\n", &digits)
	//	if digits < 0 || digits > 999 {
	//		fmt.Print("Число долно быть от 0 до 999. ")
	//	}
	//}

	var digits int
	for {
		fmt.Println("Введите число от 0 до 999")
		_, _ = fmt.Scanf("%d\n", &digits)
		if digits < 0 || digits > 999 {
			fmt.Print("Число долно быть от 0 до 999. ")
		} else {
			break
		}
	}

	var strDigits = fmt.Sprint(digits)
	if len(strDigits) == 3 {
		fmt.Printf("%c сотен\n", strDigits[0])
		fmt.Printf("%c десятков\n", strDigits[1])
		fmt.Printf("%c едениц\n", strDigits[2])
	} else if len(strDigits) == 2 {
		fmt.Printf("%c десятков\n", strDigits[0])
		fmt.Printf("%c едениц\n", strDigits[1])
	} else {
		fmt.Printf("%c едениц\n", strDigits)
	}
}

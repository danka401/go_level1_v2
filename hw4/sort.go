package main

import (
	"errors"
	"fmt"
	"log"
	"strconv"
	"strings"
)

func main() {
	lst, err := gettingData()
	if err != nil {
		log.Fatalf("Не полчилось считать числа: %v", err)
	}
	result := sort(lst)
	fmt.Println(result)
}

func gettingData() ([]int, error) {
	fmt.Println("Введите список чисел через запятую")
	var a string
	_, err := fmt.Scanln(&a)
	if err != nil {
		return nil, errors.New("Ошибка сканирования")
	}
	strin := strings.Split(a, ",")
	var lst = make([]int, len(strin))
	for i := range strin {
		lst[i], err = strconv.Atoi(strings.TrimSpace(strin[i]))
		if err != nil {
			return nil, fmt.Errorf("Ошибка парсинга значения %q", strin[i])
		}
	}
	return lst, nil
}

func sort(lst []int) []int {
	res := make([]int, len(lst))
	copy(res, lst)
	for i := 1; i < len(res); i++ {
		j := i
		for res[j-1] > res[j] && j != 0 {
			res[j-1], res[j] = res[j], res[j-1]
			j--
			if j == 0 {
				break
			}
		}
	}
	return res
}

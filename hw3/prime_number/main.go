package main

import (
	"fmt"
	"math"
	"os"
)

func main() {
	var num int
	fmt.Println("До какого числа вывести все простые числа?")
	_, err := fmt.Scanf("%d", &num)
	if err != nil {
		fmt.Println("Введено не число")
		os.Exit(1)
	}

	for i := 2; i <= num; i++ {
		isNotPrime := false
		jMax := int(math.Sqrt(float64(i)))
		for j := 2; j < jMax; j++ {
			if i != j && i%j == 0 {
				isNotPrime = true
				break
			}
		}
		if !isNotPrime {
			fmt.Printf("%d\n", i)
		}
	}
}

package main

import (
	"fmt"
	"math"
	"os"
)

func main() {
	var a, b, res float64
	var op string

	fmt.Print("Введите первое число: ")
	_, err := fmt.Scanln(&a)
	if err != nil {
		fmt.Println("Введено не число")
		os.Exit(1)
	}

	fmt.Print("Введите арифметическую операцию (+, -, *, /, ^, sqrt): ")
	fmt.Scanln(&op)

	if op != "sqrt" {
		fmt.Print("Введите второе число: ")
		_, err := fmt.Scanln(&b)
		if err != nil {
			fmt.Println("Введено не число")
			os.Exit(1)
		}
	}

	switch op {
	case "+":
		res = a + b
	case "-":
		res = a - b
	case "*":
		res = a * b
	case "/":
		if b == 0 {
			fmt.Println("На ноль делить нельзя")
			os.Exit(1)
		}
		res = a / b
	case "^":
		res = math.Pow(a, b)
	case "sqrt":
		res = math.Sqrt(a)
	default:
		fmt.Println("Операция выбрана неверно")
		os.Exit(1)
	}
	fmt.Printf("Результат выполнения операции: %f\n", res)
}

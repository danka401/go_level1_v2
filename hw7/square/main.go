package main

import (
	"fmt"
	"math"
)

type rectangle struct {
	height, width int
}

type circle struct {
	diameter int
}

type areaSizeProvider interface {
	GetAreaSize() int
}

type ShapeNameProvider interface {
	GetShapeName() string
}

type ShapeInfoProvider interface {
	areaSizeProvider
	ShapeNameProvider
}

func main() {
	circ := circle{diameter: 20}
	rctg := rectangle{height: 5, width: 7}
	printShape(circ)
	printShape(rctg)
	//fmt.Printf("Площадь {types} равна %d\n", {interfece})
}

func printShape(shapeInfoProvider ShapeInfoProvider) {
	fmt.Printf("Площадь %s равна %d.\n", shapeInfoProvider.GetShapeName(), shapeInfoProvider.GetAreaSize())
}

func (c circle) GetAreaSize() int {
	doubld := float64(c.diameter ^ 2)
	p := math.Pi * doubld

	return int(p / 4)
}

func (circle) GetShapeName() string {
	return "круга"
}

func (r rectangle) GetAreaSize() int {
	return r.height * r.width
}

func (rectangle) GetShapeName() string {
	return "четырёхугольника"
}

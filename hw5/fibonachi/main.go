package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	fmt.Println("Какое число последовательности Фибоначчи вывести?")
	var num int
	_, err := fmt.Scanf("%d\n", &num)
	if err != nil {
		log.Println("Не корректный ввод")
		os.Exit(1)
	}
	fmt.Println(fib(num))
}

func fib(num int) int {
	if num == 1 {
		return 0
	} else if num == 2 {
		return 1
	} else {
		return fib(num-1) + fib(num-2)
	}
}

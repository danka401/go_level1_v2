package main

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

func main() {
	cf := NewFibCalculator()

	for {
		{
			fmt.Println(`Если хотите выйти из программы введите "no"`)
			fmt.Println("Какое число последовательности Фибоначчи вывести?")
		}
		num, flag, err := getData()
		if err != nil {
			fmt.Println(err)
			continue
		}
		if !flag {
			break
		}
		fmt.Println(cf.Get(num))
	}
}

func getData() (int, bool, error) {
	var inStr string
	var num int
	_, err := fmt.Scanln(&inStr)

	if err != nil {
		return 0, false, errors.New("Некорректный ввод")
	}

	inStr = strings.TrimSpace(inStr)
	if inStr == "no" {
		return 0, false, nil
	}

	num, err = strconv.Atoi(inStr)

	if err != nil {
		return 0, false, errors.New("Введено не число")
	}
	return num, true, nil
}

type FibCalculator struct {
	cache map[int]int
}

func NewFibCalculator() *FibCalculator {
	return &FibCalculator{cache: map[int]int{1: 0, 2: 1}}
}

func (fc *FibCalculator) Get(num int) int {
	// допустим подсчёт числа фибоначи идёт по формуле a + b = c, тогда a это num - 2, b это num -1
	cache := fc.cache
	if v, ok := cache[num]; ok {
		return v
	}
	cache[num] = fc.Get(num-1) + fc.Get(num-2)
	return cache[num]
}
